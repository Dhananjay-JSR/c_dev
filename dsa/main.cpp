#include "iostream"
/*
struct Queue{
    int Size ;
    int front = -1;
    int Rear = -1;
    int *Q;
    explicit Queue(int Size) {
        Q = new int[Size]();
        this->Size = Size;
    }
    void enqueue(int Data){
        if((Size-1)!=Rear){
            *(Q+(++Rear)) = Data;
        }else{
            std::cout<<"Quque is Full";
        }
    }
    void dequeu(){
        if(front!=Rear){
            *(Q+(++front)) = 0;
        }
    }
    void isEmpty(){
        if(front==Rear){
            std::cout<<"Queue ios Full";
        }
    }

};
 */

struct Queue{
    int Size ;
    int front = 0;
    int Rear = 0;
    int *Q;
    explicit Queue(int Size) {
        Q = new int[Size]();
        this->Size = Size;
    }
    void enqueue(int Data){
        if((Size-1)!=Rear){
            *(Q+(++Rear)) = Data;
        }else{
            std::cout<<"Quque is Full";
        }
    }
    void dequeu(){
        if(front!=Rear){
            *(Q+(++front)) = 0;
        }
    }
    void isEmpty(){
        if(front==Rear){
            std::cout<<"Queue ios Full";
        }
    }

};

struct Queue q = Queue(7);


int main(){
    auto *test = &q;
    q.enqueue(10);
    q.enqueue(10);
    q.enqueue(10);
    q.enqueue(13);
    q.enqueue(10);
    q.enqueue(10);
    q.dequeu();
    q.dequeu();
    q.dequeu();
    q.dequeu();

    return 0;
}